# json-ld-schema-generator
Generates valid JSON-LD schema from data

## Usage

```
echo new EventSchema([
    'name' => 'Birthday Party',
    'startDate' => '2018-09-09',
    'location' => new PlaceSchema([
        'name' => 'Pizza Hut',
        'address' => new PostalAddressSchema([
            'postalCode' => 'SY2 8RU',
        ])
    ])
]);
```

