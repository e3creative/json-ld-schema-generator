<?php

namespace E3Creative\JsonLdSchemaGenerator;

abstract class BaseSchema
{
    /**
     * @var array
     */
    private $data;

    /**
     * Sets default properties and merges the child's data property with a
     * schema type declaration array
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = array_merge(['@type' => $this->getName()], $data);
    }

    /**
     * Returns the schema markup as a string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->generate()->render();
    }

    /**
     * Generates the data and adds the @context property
     *
     * @return BaseSchema
     */
    public function generate()
    {
        $this->data = $this->build();

        $this->addContext();

        return $this;
    }

    /**
     * Recursively builds an array containing the data for whole schema
     *
     * @return BaseSchema
     */
    private function build()
    {
        return array_map(function ($item) {
            return $item instanceof BaseSchema
                ? $item->build()
                : $item;
        }, $this->data);
    }

    /**
     * Adds the @context property to the top level of the schema data
     */
    private function addContext()
    {
        $this->data = array_filter(
            array_merge(
                ['@context' => 'http://schema.org'],
                $this->data
            )
        );
    }

    /**
     * Turns the schema data array into json and wraps in the ld-json script
     * tags
     *
     * @return string
     */
    private function render()
    {
        return sprintf(
            '<script type="application/ld+json">%s</script>',
            json_encode($this->data, JSON_UNESCAPED_SLASHES)
        );
    }

    /**
     * Gets the event type from the class name
     *
     * @return string
     */
    private function getName()
    {
        $classname = array_pop(explode('\\', static::class));

        return str_replace('Schema', '', $classname);
    }
}
