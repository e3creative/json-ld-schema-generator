# Changelog

## [2.0.0] - 2019-10-31
### Changed
- Updates vendor name

## [1.0.0] - 2018-09-13
### Initial release
